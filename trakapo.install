<?php
/**
 * @file
 * Provides Trakapo installation and upgrade functions.
 */

/**
 * Implements hook_update_N().
 *
 * Upgrades a 1.x version of the module to continue using UID as the main
 * identifier and convert the API key into a site_id.
 *
 * @return string
 *   Translated success string to display to the user.
 */
function trakapo_update_7200() {
  $settings = trakapo_settings();
  $settings['use_uid_as_identifier'] = TRUE;
  $settings['site_id'] = substr($settings['key'], 0, 16);

  // Remove old settings.
  unset($settings['key']);

  trakapo_settings($settings);

  return t('Upgraded Trakapo settings, Please check your configuration to ensure it\'s correct');
}

/**
 * Implements hook_update_N().
 *
 * Upgrades pre 2.5 settings to the new 2.5+ format.
 *
 * @return string
 *   Translated success string to display to the user.
 */
function trakapo_update_7201() {
  $settings = trakapo_settings();
  $new_settings = array(
    'updated' => time(),
    'basic' => array(
      'site_id' => $settings['site_id'],
      'use_uid_as_identifier' => $settings['use_uid_as_identifier'],
      'track_all_emails' => $settings['track_all_emails'],
    ),
    'limiting' => array(
      'exclude_roles' => $settings['exclude_roles'],
      'pages_visibility_mode' => $settings['pages_visibility_mode'],
      'pages_visibility_paths' => $settings['pages_visibility_paths'],
    ),
  );

  // Remove old settings.
  unset($settings['site_id']);
  unset($settings['use_uid_as_identifier']);
  unset($settings['track_all_emails']);
  unset($settings['exclude_roles']);
  unset($settings['pages_visibility_mode']);
  unset($settings['pages_visibility_paths']);

  // Save settings.
  trakapo_settings($new_settings + $settings);

  return t('Upgraded Trakapo settings, Please check your configuration to ensure it\'s correct');
}

/**
 * Implements hook_update_N().
 *
 * Removes uid identifier setting.
 *
 * @return string
 *   Translated success string to display to user.
 */
function trakapo_update_7202() {
  $settings = trakapo_settings();
  unset($settings['basic']['use_uid_as_identifier']);
  trakapo_settings($settings);

  return t('Upgraded Trakapo settings, Please check your configuration to ensure it\'s correct');
}
