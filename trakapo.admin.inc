<?php
/**
 * @file
 * Provides Trakapo administration settings pages.
 */

/**
 * Form callback for the API keys form.
 *
 * @param array $form
 *   Drupal form object for the settings form to be rendered.
 *
 * @return array
 *   Completed Drupal settings form for Trakapo.
 */
function trakapo_settings_form($form) {
  $settings = trakapo_settings();

  $visibility_options = array(
    TRAKAPO_VISIBILITY_NOTLISTED => t('All pages except those listed'),
    TRAKAPO_VISIBILITY_LISTED => t('Only the listed pages'),
  );
  $visibility_title = t('Pages');
  $visibility_desc = t(
    'Specify pages by using their paths. Enter one path per line. The "*" character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.',
    array(
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>',
    )
  );

  if (module_exists('php')) {
    $visibility_options += array(TRAKAPO_VISIBILITY_PHP => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
    $visibility_title = t('Pages or PHP code');
    $visibility_desc .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
  }

  $form['trakapo'] = array(
    '#tree' => TRUE,
    'updated' => array(
      '#type' => 'hidden',
      '#value' => time(),
    ),
    'basic' => array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#title' => t('Basic Settings'),
      '#description' => t('Basic settings needed to get Trakapo up and running on your Drupal site.'),
      'site_id' => array(
        '#type' => 'textfield',
        '#title' => t('Site ID'),
        '#default_value' => $settings['basic']['site_id'],
      ),
      'track_all_emails' => array(
        '#type' => 'checkbox',
        '#title' => t('Try to identify on any email'),
        '#description' => t('Try to detect any situation where an email is entered and identify the user with that email. Do not use if you expect a user to input an email other than their own at any point. This is only for unregistered users as user registration is a much better means of identification.'),
        '#default_value' => $settings['basic']['track_all_emails'],
      ),
    ),
    'limiting' => array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('When should Trakapo be loaded?'),
      '#description' => t('These settings allow you to specify cases where Trakapo is or isn\'t loaded into the page.'),
      'exclude_roles' => array(
        '#type' => 'checkboxes',
        '#title' => t('Do not add for roles'),
        '#description' => t('Users who have any of the selected roles will not be tracked.'),
        '#default_value' => $settings['limiting']['exclude_roles'],
        '#options' => user_roles(),
      ),
      'pages_visibility_mode' => array(
        '#type' => 'radios',
        '#title' => t('Adding tracking on specific pages'),
        '#options' => $visibility_options,
        '#default_value' => $settings['limiting']['pages_visibility_mode'],
      ),
      'pages_visibility_paths' => array(
        '#type' => 'textarea',
        '#title' => '<span class="element-invisible">' . $visibility_title . '</span>',
        '#default_value' => $settings['limiting']['pages_visibility_paths'],
        '#description' => $visibility_desc,
      ),
    ),
  );

  return system_settings_form($form);
}
