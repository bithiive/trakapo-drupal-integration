Trakapo Drupal Integration Plugin
=================================
Basic Trakapo integration for Drupal 7. Also include an integration module for Drupal Commerce.

Installation
============
Clone this repository (or add it as a git submodule) to a modules folder in your Drupal install, you can then enable it in the administrator module settings or via Drush.

After the module is enabled, it will not start functioning until you add your Trakapo site ID into the configuration pages (Under the "Configuration" > "Web Services" > "Trakapo" menu). Here you can customise other aspects of the plugin.

Testing
=======
No unit testing available yet...
